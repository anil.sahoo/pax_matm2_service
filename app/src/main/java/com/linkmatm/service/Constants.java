package com.linkmatm.service;

import android.content.Intent;

public class Constants {
    public static String token = "";
    public static String user_id = "";
//    public static final String TEST_URL =  "https://matm.iserveu.tech";
    public static final String TEST_URL =  "https://matm.iserveu.online";

    public static final String TEST_PATH =  "";
//    public static final String TEST_PATH =  "/MATM2LIVE";


//    public static final String BASE_URL = "https://coreuat-zwqcqy3qmq-el.a.run.app";
    public static final String BASE_URL = "https://mobile.9fin.co.in/";


    //Constant Data for MPOS
    public static boolean isSL = false;
    public static String Transaction_Type = "9C0101";
    public static String Transaction_Type_Enquery ="9C0131";
    public static String Transaction_Currency_Code = "5F2A020356";
    public static String CurrencyExponent = "5F360102";
    public static String AmountOthers = "9F0306000000000000";
    public static String PINEncryptionType = "02020101";
    public static String DataEncryptionType_No = "02050100";
    public static String PINEncryptionKeyIdx = "02030120";
    public static String DataEncryptionType = "02050101";
    public static String DataEncryptionKeyIdx = "02060125";
    public static String PINBlockMode = "02040100";
    public static String TransactionProcessingMode = "02090101"; //02090100
    public static String CardEntryMode = "02140107";
    public static String FallBackAllowFlag = "02070101";
    public static String PosEntryMode = "9F39020051";
    public static String TerminalCapabilities = "9F3303E060C8";
    public static String RequestPinBlockAuto = "02150101";
    public static String OnlinePinInput = "03150101";

    public static String TerminalType = "9F3501";
    public static String AdditionalTerminalCapabilities = "9F4005";


    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String encryptedString = "encryptedString";
    public static final String devicename = "devicename";
    public static final String devicemac = "devicemac";


    public static String deviceSerialNo ="";

    public static String tokenFromCoreApp ="";
    public static String userNameFromCoreApp ="";
    public static String applicationType ="";
    public static String LogOut = ""; //0 --For logout
    public static String BlueToothPairFlag = "1";   // 1--> for allow
    public static String Matm1BluetoothFlag = "0"; // 1--->pair Bluetooth

    public static String transactionAmount="0";
    public static String transactionType="";
    public static String paramA="";
    public static String paramB="";
    public static String paramC="";
    public static String encryptedData="";
    public static String balanceEnquiry="0";
    public static String cashWithdrawal="1";
    public static String transactionResponse="";
    public static String loginID ="";
    public static String responseData ="responseData";
    public static int REQUEST_CODE= 99;

    public static Intent intentData;
    public static String isPos = "0";

    public static String initPos = "0";
    public static String statusPos = "1";
    public static String error2Pos = "2";
    public static String errorPos = "3";
    public static String eventPos = "4";


    public static String latitude="";
    public static String longitude="";
    public static String activityName="";

    public static void initResult(){
        intentData = new Intent();
        isPos = "0";
    }

    public static boolean emvParam = false;
    public static boolean clsParam = false;
    public static String emvFile = "";

}
