package com.linkmatm.service.selfUpdate;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.linkmatm.service.R;

public class DownloadActivity extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        textView = findViewById(R.id.download);
        Bundle b = getIntent().getExtras();
//        new DownloadNewVersion(this).execute();
        new DownloadNewVersion(this, b.getString("AppLink"), textView).execute();
    }
}