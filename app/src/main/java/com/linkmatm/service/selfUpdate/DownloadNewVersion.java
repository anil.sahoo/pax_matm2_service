package com.linkmatm.service.selfUpdate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.linkmatm.service.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadNewVersion extends AsyncTask<String, Integer, Boolean> {
    private static final String TAG = DownloadNewVersion.class.getSimpleName();
    private Context context;
    private String urlString;
    private TextView textView;

    public DownloadNewVersion(Context context, String urlString, TextView textView) {
        this.context = context;
        this.urlString = urlString;
        this.textView = textView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        textView.setText("Downloading...");
    }

    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        String msg = "";
        if (progress[0] > 99) {
            msg = "Finishing... ";
        } else {
            msg = "Downloading... " + progress[0] + "%";
        }
        textView.setText(msg);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        textView.setText("Downloaded");
        if (result) {
            Toast.makeText(context.getApplicationContext(), "Downloaded", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context.getApplicationContext(), "Error: Try Again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected Boolean doInBackground(String... arg0) {
        Boolean flag = false;
        try {
            URL url = new URL(urlString);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
            c.connect();
            String PATH = Environment.getExternalStorageDirectory() + "/Download/";
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, "matm2Service.apk");
            if (outputFile.exists()) {
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);
            InputStream is = c.getInputStream();
            int total_size = 5281692;//size of apk
            byte[] buffer = new byte[1024];
            int len1 = 0;
            int per = 0;
            int downloaded = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
                downloaded += len1;
                per = (int) (downloaded * 100 / total_size);
                publishProgress(per);
            }
            fos.close();
            is.close();
            OpenNewVersion(PATH);
            flag = true;
        } catch (Exception e) {
            Log.e(TAG, "Update Error: " + e.getMessage());
            e.printStackTrace();
            flag = false;
        }
        return flag;
    }

    void OpenNewVersion(String location) {
        Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", new File(location + "matm2Service.apk"));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            uri = Uri.fromFile(new File(location + "matm2Service.apk"));
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
//        deleteApp();
        ((Activity)context).finish();
    }

    void deleteApp(){
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:"+BuildConfig.APPLICATION_ID));
        context.startActivity(intent);
    }

}
