package com.linkmatm.service;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linkmatm.service.Utils.Dialog;
import com.linkmatm.service.Utils.PreferenceUtility;
import com.linkmatm.service.Utils.ResultEvent;
import com.linkmatm.service.Utils.Session;
import com.linkmatm.service.Utils.Tools;
import com.linkmatm.service.emvDownload.HomeActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.device.DeviceInfo;
import com.paxsz.easylink.listener.SearchDeviceListener;


import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static android.content.ContentValues.TAG;

public class BluetoothActivity extends AppCompatActivity {

    LinearLayout pairedDeviceLayout, progress_container, llAvailableDevice;
    TextView pairedDeviceTitle, tvProgress;
    ListView availableDevicesList;
    CoordinatorLayout container;
    private static final String DEVICE_NAME = "DEVICE_NAME";
    private static final String DEVICE_MAC = "DEVICE_MAC";
    private String pairedDeviceName;
    private String pairedDeviceMac, terminal_address = "Bhubaneswer,Odisha";
    private ImageView refresh;
    private RotateAnimation anim;
    private ArrayList<HashMap<String, String>> deviceInfo = new ArrayList<>();
    private EasyLinkSdkManager manager;
    // private Handler handler;
    //private boolean isStopBtn = false;
    ProgressDialog progressDialog;


    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    String currentName = "";
    String currentMac;
    Session session;
    PreferenceUtility prefUtlity;
    Toolbar toolbar;

    ProgressBar progress_bar;
    ImageView iv_pending;
    Button btnRetry;

    SharedPreferences sp;
    public static final String MATM_PREF = "matmPref";
    public static final String MATM_USER = "userPref";
    public static final String MATM_TOKEN = "tokenPref";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        sp = getSharedPreferences(MATM_PREF, Context.MODE_PRIVATE);

        FirebaseApp.initializeApp(BluetoothActivity.this);
        prefUtlity = new PreferenceUtility(BluetoothActivity.this);
        progressDialog = new ProgressDialog(BluetoothActivity.this);

        //setUpGClient();
        session = new Session(BluetoothActivity.this);
        manager = EasyLinkSdkManager.getInstance(this);

        //-------------
        //CheckFirebaseDB();


        if (Constants.LogOut.equalsIgnoreCase("0")) {
            logoutDevice();
        }

        if (Constants.applicationType.equalsIgnoreCase("CORE")) {

        } else {
            Constants.user_id = Constants.loginID;

           /* String prefUser = sp.getString(MATM_USER, "");
            if (prefUser.equals("") || !prefUser.equals(Constants.user_id)){
                //fetchToken
                getSdkToken();
            } else {
                Constants.token = sp.getString(MATM_TOKEN, "");
            }*/

            getUserAuthToken();

        }
        pairedDeviceLayout = findViewById(R.id.pairedDeviceLayout);
        pairedDeviceTitle = findViewById(R.id.pairedDeviceTitle);
        availableDevicesList = findViewById(R.id.availableDevicesList);
        container = findViewById(R.id.container);

        progress_container = findViewById(R.id.progress_container);
        progress_bar = findViewById(R.id.progress_bar);
        llAvailableDevice = findViewById(R.id.llAvailableDevice);
        iv_pending = findViewById(R.id.iv_pending);
        tvProgress = findViewById(R.id.tvProgress);
        btnRetry = findViewById(R.id.btnRetry);

        //handler = new Handler();
        initView();
        setListener();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.startAnimation(anim);
                deviceInfo.clear();
                BluetoothActivity.this.updateBluetoothList();
                manager.searchDevices(new CustomDeviceSearchListener(), 10000);
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void CheckFirebaseDB() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("matmService2").document("accessPermission");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if (document.contains("isActive")) {

                                Boolean value = document.getBoolean("isActive");

                                if (value) {

                                    Toast.makeText(BluetoothActivity.this, "True", Toast.LENGTH_SHORT).show();


                                } else {
                                    //showStatusFailure("Unauthorized, please call or email helpdesk to continue service.");
                                    //Toast.makeText(BluetoothActivity.this, "False", Toast.LENGTH_SHORT).show();
                                    showAlert("Sorry , You have not access to use Application.");

                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(BluetoothActivity.this, "Error", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    private void initView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        toolbar = findViewById(R.id.bluetooth_toolbar);
        resetPairedDeviceUI();
        refresh = findViewById(R.id.refresh);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothActivity.this.onBackPressed();
            }
        });
        anim = new RotateAnimation(360.0f, 0.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(1000);
        refresh.setAnimation(anim);
        deviceInfo.clear();
        updateBluetoothList();
        manager.searchDevices(new CustomDeviceSearchListener(), 40000);


        String devName = "";
        String devceMac = "";
        devName = prefUtlity.getString("DEVICE_NAME", "");
        devceMac = prefUtlity.getString("DEVICE_MAC", null);

        if (!(devName == null || devName.equals("") || devceMac == null || devceMac.equals(""))) {
            pairedDeviceLayout.setVisibility(View.VISIBLE);
            pairedDeviceTitle.setText(devName);
            if (!manager.isConnected()) {
                if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                    progress_container.setVisibility(View.VISIBLE);
                }
                checkDeviceStatus(devName, devceMac);
            }
        }


      /*  if (!(devName == null || devName.equals("") || devceMac == null || devceMac.equals(""))) {
            pairedDeviceLayout.setVisibility(View.VISIBLE);
            pairedDeviceTitle.setText("D180-"+devName);
            if(!manager.isConnected()){
            checkDeviceStatus(devName, devceMac);
            }
        }*/
    }

    private void setListener() {
        availableDevicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                BluetoothActivity.this.onItemClick(pos);
            }
        });
    }


    private void resetPairedDeviceUI() {
        if (!manager.isConnected(DeviceInfo.CommType.BLUETOOTH)) {
            pairedDeviceLayout.setVisibility(View.GONE);
            if (pairedDeviceName != null && pairedDeviceMac != null) {
                HashMap<String, String> map = new HashMap<>();
                map.put(DEVICE_NAME, pairedDeviceName);
                map.put(DEVICE_MAC, pairedDeviceMac);
                deviceInfo.add(map);
                pairedDeviceName = "";
                pairedDeviceMac = "";
            }
            return;
        }
        pairedDeviceName = manager.getConnectedDevice().getDeviceName();
        pairedDeviceMac = manager.getConnectedDevice().getIdentifier();
        pairedDeviceLayout.setVisibility(View.VISIBLE);
        /*.if(pairedDeviceName.contains("D180-")){
            pairedDeviceName.replace("","")
        }*/

        pairedDeviceTitle.setText(pairedDeviceName);

        //remove paired device from available devices list
        for (int i = 0; i < deviceInfo.size(); i++) {
            if (pairedDeviceName.equals(deviceInfo.get(i).get(DEVICE_NAME))
                    && pairedDeviceMac.equals(deviceInfo.get(i).get(DEVICE_MAC))) {
                deviceInfo.remove(i);
                break;
            }
        }
    }

    private void updateBluetoothList() {
        final BluetoothListAdapter adapter = new BluetoothListAdapter(this, deviceInfo);
        availableDevicesList.setAdapter(adapter);
        availableDevicesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    /**
     * BluetoothList  On Item click ---- Rajesh
     */

    private void onItemClick(int pos) {
        currentName = deviceInfo.get(pos).get(DEVICE_NAME);
        currentMac = deviceInfo.get(pos).get(DEVICE_MAC);
        manager.disconnect();


        checkDeviceStatus(currentName, currentMac);
        manager.stopSearchingDevice();

    }

    private void saveDeviceInfo(String currentName, String currentMac) {

        prefUtlity.saveString("DEVICE_NAME", currentName);
        prefUtlity.saveString("DEVICE_MAC", currentMac);

    }

    // Pair device Connect  -------Rajesh
    private void connectDevice(String deviceName, String deviceMac) {
        DeviceInfo deviceInfo = new DeviceInfo(DeviceInfo.CommType.BLUETOOTH, "D180-" + deviceName, deviceMac);
        int ret = manager.connect(deviceInfo);
       /* if (isDestroyed()) {
            return;
        }*/

        if (ret == ResponseCode.EL_RET_OK) {
            Log.i("log", "connect success");
            Log.i("log", deviceName);
            pairSuccessResponse();
        } else {
            if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress_bar.setVisibility(View.GONE);
                        tvProgress.setText("Device not connected, Please Make sure the device is nearby or Switched On");
                        iv_pending.setVisibility(View.VISIBLE);
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                hideLoader();
                //showUserOnboardStatusFailure("Oops! Something went wrong.");
            }

        }
    }

   /* private void pairFailureResponse() {
        //refresh.clearAnimation();

    }*/

    private void pairSuccessResponse() {

        // refresh.clearAnimation();
        String devName = "";
        devName = prefUtlity.getString("DEVICE_NAME", "");
        if (!currentName.equalsIgnoreCase("")) {
            System.out.println("Hi");
            if (devName.equalsIgnoreCase(currentName.replace("D180-", "").trim())) {
                String deviceNamee = prefUtlity.getString("DEVICE_NAME", null);
                String devceMacc = prefUtlity.getString("DEVICE_MAC", null);
                // Generate TPK TDK by------- RAJESH
                deviceRegisteration(deviceNamee, devceMacc);

            } else {
                checkDeviceStatus(currentName, currentMac);
            }
        } else {

            String deviceNamee = prefUtlity.getString("DEVICE_NAME", null);
            String devceMacc = prefUtlity.getString("DEVICE_MAC", null);
            // Generate TPK TDK by------- RAJESH
            deviceRegisteration(deviceNamee, devceMacc);
        }
    }


    /**
     * Device Search
     */
    private class CustomDeviceSearchListener implements SearchDeviceListener {
        @Override
        public void discoverOneDevice(DeviceInfo deviceInfo) {
            addDevice(deviceInfo);
        }

        @Override
        public void discoverComplete() {
            refresh.clearAnimation();
        }
    }

    /**
     * set Bluetooth adapter
     */

    public class BluetoothListAdapter extends BaseAdapter {

        private Context context;

        public BluetoothListAdapter(Context context, ArrayList<HashMap<String, String>> deviceInfo) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return deviceInfo.size();
        }

        @Override
        public Object getItem(int position) {
            return deviceInfo.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.bluetooth_list, parent, false);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.bluetoothDeviceName);// display text
            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            if (deviceInfo.size() > 0) {
                tv.setText(deviceInfo.get(position).get(DEVICE_NAME));
                imageView.setImageResource(R.drawable.icn_bluetooth);
            }
            return view;
        }
    }


    private void addDevice(DeviceInfo deviceInf) {
        //avoid add the same device
        if (deviceInf.getDeviceName().equals(pairedDeviceName) && deviceInf.getIdentifier().equals(pairedDeviceMac)) {
            return;
        }
        for (int i = 0; i < deviceInfo.size(); i++) {
            if (deviceInf.getDeviceName().equals(deviceInfo.get(i).get(DEVICE_NAME)) && deviceInf.getIdentifier().equals(deviceInfo.get(i).get(DEVICE_MAC))) {
                return;
            }
        }

        HashMap<String, String> map = new HashMap<>();
        map.put(DEVICE_NAME, deviceInf.getDeviceName());
        map.put(DEVICE_MAC, deviceInf.getIdentifier());
        deviceInfo.add(map);
        updateBluetoothList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent data = new Intent();
        setResult(Activity.RESULT_OK, data);
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1001) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "requestPermissions success");
            } else {
                Log.d(TAG, "requestPermissions fail");
            }
        }
        int permissionLocation = ContextCompat.checkSelfPermission(BluetoothActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //-----------Pravin API for Device Mapping ------------------------

    public void checkDeviceStatus(final String currentName, final String currentMac) {
        showLoader();
        try {
            JSONObject obj = new JSONObject();
            obj.put("deviceSlNo", currentName.replace("D180-", "").trim());
            obj.put("userName", Constants.user_id);
            //obj.put("userId", Integer.valueOf(Constants.user_id));

            Log.e(TAG, "checkDeviceStatus: calling");


            AndroidNetworking.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/matmmapping/mapwithusername")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "checkDeviceStatus onResponse: " + response);
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                if (status.equalsIgnoreCase("0")) {

                                    // pairing pax with blueetooth........Rajesh
                                    prefUtlity.saveString("DEVICE_NAME", currentName.replace("D180-", "").trim());
                                    prefUtlity.saveString("DEVICE_MAC", currentMac.trim());

                                    new PairDevice().execute(currentName, currentMac);

                                } else {
                                    hideLoader();

                                    String responseString = obj.getString("desc");

                                    showUserOnboardStatusFailure(responseString + "\nplease press OK to proceed further.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showUserOnboardStatusFailure("Unauthorized device paired please call or email helpdesk to continue ATM transactions.");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("Error  " + anError.getErrorDetail());
                            hideLoader();
                            String errorStr = anError.getErrorBody();
                            Log.e(TAG, "onError: map error " + errorStr);
                            try {
                                JSONObject obj = new JSONObject(errorStr);
                                String status = obj.getString("status");
                                if (status.equalsIgnoreCase("1")) {
                                    String sttsDesc = obj.getString("errorMessage");
                                    String message = "";
                                    message = obj.optString("message");
                                    // Toast.makeText(BluetoothActivity.this, sttsDesc, Toast.LENGTH_LONG).show();
                                    if (sttsDesc.equalsIgnoreCase("user name can't be null")) {
                                        showUserOnboardStatusFailureNullResponse(sttsDesc);

                                    } else {
                                        showUserOnboardStatusFailure(sttsDesc);
                                    }

                                } else {
                                    Toast.makeText(BluetoothActivity.this, "Oops!! Server error.", Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showUserOnboardStatusSuccess(final String statusDesc) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(BluetoothActivity.this);
        builder1.setMessage(statusDesc);
        builder1.setTitle("Alert");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dialogDismiss();
                        onBackPressed();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public void showExchangeAlert(final String currentName, final String currentMac, final long time) {
        final String msg = "Key Exchange Failed";
        try {
            android.app.AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new android.app.AlertDialog.Builder(BluetoothActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new android.app.AlertDialog.Builder(BluetoothActivity.this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(msg)
                    .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resonToRetry(currentName, currentMac, time, msg);
                            dialog.dismiss();
                        }
                    });
/*
            alertbuilderupdate.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            prefUtlity.saveString("DEVICE_NAME", "");
                            prefUtlity.saveString("DEVICE_MAC", "");
                            manager.disconnect();
                            dialog.dismiss();
                            onBackPressed();
                        }
                    });
*/
            android.app.AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }


    private void showUserOnboardStatusFailure(final String statusDesc) {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(BluetoothActivity.this);
            builder1.setMessage(statusDesc);
            builder1.setTitle("Alert");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //dialogDismiss();
                            String devName = "";
                            String devceMac = "";
                            prefUtlity.saveString("DEVICE_NAME", "");
                            prefUtlity.saveString("DEVICE_MAC", "");
                            manager.disconnect();
                            onBackPressed();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    // --------------Go to setting Page
    private void showUserOnboardStatusFailureNullResponse(final String statusDesc) {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(BluetoothActivity.this);
            builder1.setMessage("Go to Setting page  to pair your Device.");
            builder1.setTitle("Alert");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //dialogDismiss();
                            String devName = "";
                            String devceMac = "";
                            prefUtlity.saveString("DEVICE_NAME", "");
                            prefUtlity.saveString("DEVICE_MAC", "");
                            manager.disconnect();
                            onBackPressed();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    //call CurrentDateTime
    public void deviceRegisteration(final String currentName, final String currentMac) {
        AndroidNetworking.get("https://getnptimestamp.iserveu.website/getTimestamp")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "deviceRegisteration onResponse: " + response);
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            if (status.equalsIgnoreCase("0")) {
                                String milisec = obj.getString("milisecond");

                                long current_milisec = 0;
                                if (!milisec.equalsIgnoreCase("")) {
                                    current_milisec = Long.parseLong(milisec);
                                }
                                long injectTime = prefUtlity.getLong("INJECTED_TIME", 0);

                                if (injectTime > 0) {
                                    String device_name = prefUtlity.getString("DEVICE_NAME", null);
                                    long saved_time = prefUtlity.getLong("INJECTED_TIME", 0);
                                    Calendar saved_calendar = Calendar.getInstance();
                                    saved_calendar.setTimeInMillis(saved_time);

                                    long diff = current_milisec - saved_calendar.getTimeInMillis();

                                    float dayCount = (float) diff / (24 * 60 * 60 * 1000);
                                    if (device_name.equalsIgnoreCase(currentName.replace("D180-", ""))) {

                                        injectKeys(currentName, currentMac, current_milisec);
                                       /* if (dayCount >= 1) {
                                            injectKeys(currentName, currentMac, current_milisec);
                                        } else {

                                            saveDeviceInfo(currentName, currentMac);
                                            runInBackground(new Runnable() {
                                                @Override
                                                public void run() {
                                                    connectDevice(currentName, currentMac);
                                                }
                                            });
                                        }*/
                                    } else {
                                        injectKeys(currentName, currentMac, current_milisec);
                                    }

                                } else {
                                    injectKeys(currentName, currentMac, current_milisec);
                                }
                            } else {
                                Toast.makeText(BluetoothActivity.this, "Time stamp not generated .", Toast.LENGTH_SHORT).show();
                                //dialogDismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(BluetoothActivity.this, "Time stamp not generated .", Toast.LENGTH_SHORT).show();

                            // dialogDismiss();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        // dialogDismiss();
                        Toast.makeText(BluetoothActivity.this, "Server Error. Time stamp not generated .", Toast.LENGTH_SHORT).show();

                    }
                });
    }


    private void resonToRetry(final String currentName, final String currentMac, final long time, final String msg) {
        showLoader();
        String Url = "";
        if (Constants.applicationType.equalsIgnoreCase("CORE")) {  //For app
            Url = Constants.TEST_URL + Constants.TEST_PATH + "/generateTPKandTDK/" + currentName.replace("D180-", "").trim();

        } else {
            //For SDK user
            Url = Constants.TEST_URL + Constants.TEST_PATH + "/generateTPKandTDK/" + currentName.replace("D180-", "").trim() + "/" + Constants.user_id;

        }
        AndroidNetworking.post(Url)
                .setPriority(Priority.HIGH)
                .addQueryParameter("reasonToretry", msg)
                .addHeaders("Authorization", Constants.token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("MPOS :", response.toString());
                        try {
                            if (response != null) {

                                JSONObject jObj = new JSONObject(response.toString());


                                String tpkString = jObj.getString("tpk");
                                String tdkString = jObj.getString("tdk");

                                int TPK = Tools.testWriteKey_EncryptTPK(manager, tpkString);
                                int TDK = Tools.testWriteKey_EncryptTDK(manager, tdkString);

                                hideLoader();

                                if (TPK == 0 && TDK == 0) {
                                    saveDeviceInfo(currentName, currentMac);
                                    resetPairedDeviceUI();
                                    updateBluetoothList();
                                    prefUtlity.saveString("DEVICE_NAME", currentName.replace("D180-", "").trim());
                                    prefUtlity.saveLong("INJECTED_TIME", time);
                                    Constants.BlueToothPairFlag = "1";

                                    if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                                        if (PosActivity.isBlueToothConnected(BluetoothActivity.this)) {
                                            finish();
                                        }
                                    } else {
                                        showUserOnboardStatusSuccess("Bluetooth device paired successfully");
                                    }
                                } else {
                                    callEmv("Key Exchange Failed");
                                }
                            }

                            // }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        hideLoader();
                        Log.e("MPOS :", anError.getErrorBody());
                        String response = anError.getErrorBody();
                        //{"status":-1,"statusDesc":"Device is not registered with us."}
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equalsIgnoreCase("-1")) {
                                String desc = jsonObject.getString("statusDesc");
                                showUserOnboardStatusFailure(desc);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    private void callEmv(String statusDesc) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(BluetoothActivity.this);
        builder1.setMessage(statusDesc);
        builder1.setTitle("Alert");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Refresh Device",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        manager.disconnect();
                        Intent intent = new Intent(BluetoothActivity.this, HomeActivity.class);
                        startActivity(intent);
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public void injectKeys(final String currentName, final String currentMac, final long time) {
        showLoader();
        String Url = "";
        if (Constants.applicationType.equalsIgnoreCase("CORE")) {  //For app
            Url = Constants.TEST_URL + Constants.TEST_PATH + "/generateTPKandTDK/" + currentName.replace("D180-", "").trim();

        } else {
            //For SDK user
            Url = Constants.TEST_URL + Constants.TEST_PATH + "/generateTPKandTDK/" + currentName.replace("D180-", "").trim() + "/" + Constants.user_id;

        }
//        String Url = Constants.TEST_URL+"/TESTPROD/generateTPKandTDK/"+currentName.replace("D180-","").trim()+"/"+ Constants.loginID;
        Log.e(TAG, "injectKeys: url " + Url);
        Log.e(TAG, "injectKeys: toke " + Constants.token);
        AndroidNetworking.post(Url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", Constants.token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            if (response != null) {

                                JSONObject jObj = new JSONObject(response.toString());
                                //String jsonSts = jObj.getString("status");
                               /* if(jsonSts.equalsIgnoreCase("-1")){
                                    String stsDesc = jObj.getString("statusDesc");
                                    showAlert(stsDesc);

                                }else{*/
                                //saveDeviceInfoForMapping(currentName, currentMac);


                                String tpkString = jObj.getString("tpk");
                                String tdkString = jObj.getString("tdk");

//                                int TMK = Tools.testWriteKey_TMK(manager);
                                int TPK = Tools.testWriteKey_EncryptTPK(manager, tpkString);
                                int TDK = Tools.testWriteKey_EncryptTDK(manager, tdkString);
                                //int TDK = Tools.testWriteKey_EncryptTDKWithKCV(manager, tdkString);

                                Log.e(TAG, "onResponse: TPK " + TPK);
                                Log.e(TAG, "onResponse: TDK " + TDK);
//                                Log.e(TAG, "onResponse: TMK "+TMK );

                                hideLoader();

                                if (TPK == 0 && TDK == 0) {
                                    saveDeviceInfo(currentName, currentMac);
                                    resetPairedDeviceUI();
                                    updateBluetoothList();
                                    prefUtlity.saveString("DEVICE_NAME", currentName.replace("D180-", "").trim());
                                    prefUtlity.saveLong("INJECTED_TIME", time);
                                    Constants.BlueToothPairFlag = "1";

                                    if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                                        if (PosActivity.isBlueToothConnected(BluetoothActivity.this)) {
                                            //progress_container.setVisibility(View.GONE);
                                            // Intent intent = new Intent(BluetoothActivity.this, PosActivity.class);
                                            //startActivity(intent);
                                            finish();
                                        }
                                    } else {
                                        showUserOnboardStatusSuccess("Bluetooth device paired successfully");
                                    }
                                } else {
//                                    showUserOnboardStatusFailure("Key Exchange Failed");
                                    showExchangeAlert(currentName, currentMac, time);
                                }
                            }

                            // }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        hideLoader();
                        Log.e(TAG, "onError: " + anError.getErrorBody());
                        String response = anError.getErrorBody();
                        //{"status":-1,"statusDesc":"Device is not registered with us."}
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equalsIgnoreCase("-1")) {
                                String desc = jsonObject.getString("statusDesc");
                                showUserOnboardStatusFailure(desc);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    //-----------------


/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }

    }*/


    private void getSdkToken(){
        showLoader();
        String url = "https://coreuat-zwqcqy3qmq-el.a.run.app/api/getAuthenticateData";

        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData", Constants.encryptedData);
            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "onResponse: get token " + response);
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if (status.equalsIgnoreCase("success")) {
//                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
//                                    userToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpc3V0ZXN0IiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNjI3MDkxNzA4MTA2LCJleHAiOjE2MjcwOTM1MDh9.cwFCmIvq8n0uTKHfKOvXfivHfGvhNQRkITyE5J4mBSpvo-H6hdIYxJI9m9cv7ZB1sraWstXv09fdIMoguhFIVg";
                                    //String userToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTkxODU5NjMzLCJleHAiOjE1ODgxOTM2NTl9.0tZb8XrRIkFJ3ZamNuoL3n5OkqXvXPc4xU2EoJzbivrOOlg1jMse_WzpJtZDRH9-ESKBBOlfQ680V8U09WwUKg";
                                    Constants.token = userToken;

                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(MATM_USER, userToken);
                                    editor.putString(MATM_TOKEN, Constants.user_id);
                                    editor.apply();

                                    // getUserId(userToken,"https://mobile.9fin.co.in/user/user_details");

                                    hideLoader();

                                } else {
                                    showAlert("Error");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showAlert("Invalid Encrypted Data");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserAuthToken() {
        showLoader();
        String url = Constants.BASE_URL + "/api/getAuthenticateData";
        //String url = "https://newapp.iserveu.online/AEPS2NEW"+"/api/getAuthenticateData";

        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData", Constants.encryptedData);
            obj.put("retailerUserName", Constants.loginID);
            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "onResponse: get token " + response);
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if (status.equalsIgnoreCase("success")) {
//                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
//                                    userToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpc3V0ZXN0IiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNjI3MDkxNzA4MTA2LCJleHAiOjE2MjcwOTM1MDh9.cwFCmIvq8n0uTKHfKOvXfivHfGvhNQRkITyE5J4mBSpvo-H6hdIYxJI9m9cv7ZB1sraWstXv09fdIMoguhFIVg";
                                    //String userToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTkxODU5NjMzLCJleHAiOjE1ODgxOTM2NTl9.0tZb8XrRIkFJ3ZamNuoL3n5OkqXvXPc4xU2EoJzbivrOOlg1jMse_WzpJtZDRH9-ESKBBOlfQ680V8U09WwUKg";
                                    Constants.token = userToken;

                                    // getUserId(userToken,"https://mobile.9fin.co.in/user/user_details");

                                    hideLoader();

                                } else {
                                    showAlert("Error");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showAlert("Invalid Encrypted Data");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showAlert(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();

            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void showAlertDeviceIspaired(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                prefUtlity.saveString("DEVICE_NAME", "");
                prefUtlity.saveString("DEVICE_MAC", "");
                prefUtlity.saveLong("INJECTED_TIME", 0);
                prefUtlity.clearAll(BluetoothActivity.this);
                manager.disconnect();
                finish();

            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void logoutDevice() {
        prefUtlity.saveString("DEVICE_NAME", "");
        prefUtlity.saveString("DEVICE_MAC", "");
        prefUtlity.saveLong("INJECTED_TIME", 0);
        prefUtlity.clearAll(BluetoothActivity.this);
        manager.disconnect();
        finish();
        Constants.LogOut = "";
    }
    //-------------------------------------------------------

    public class PairDevice extends AsyncTask<String, String, String> {
        @Override
        public void onPreExecute() {
            showLoader();
        }


        @Override
        protected String doInBackground(String... params) {
            String pairName = params[0];
            String macAdd = params[1];
            connectDevice(pairName, macAdd);

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }


    public void showLoader() {
        try {
            String devName = "";
            String devceMac = "";
            devName = prefUtlity.getString("DEVICE_NAME", "");
            devceMac = prefUtlity.getString("DEVICE_MAC", null);
            if (!isFinishing() && !isDestroyed()) {
                if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                    if (devName == null || devName.equals("") || devceMac == null || devceMac.equals("")) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            System.out.println("Showing....");
                        } else {
                            progressDialog.setMessage("connecting...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                        }
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        System.out.println("Showing....");
                    } else {
                        progressDialog.setMessage("connecting...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    }

                }
            }
        } catch (Exception e) {


        }
    }


    public void hideLoader() {
        try {
            String devName = "";
            String devceMac = "";
            devName = prefUtlity.getString("DEVICE_NAME", "");
            devceMac = prefUtlity.getString("DEVICE_MAC", null);
            if (!isFinishing() && !isDestroyed()) {
                if (Constants.activityName.equalsIgnoreCase("mATM2")) {
                    if (devName == null || devName.equals("") || devceMac == null || devceMac.equals("")) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        } catch (Exception e) {


        }
    }

}