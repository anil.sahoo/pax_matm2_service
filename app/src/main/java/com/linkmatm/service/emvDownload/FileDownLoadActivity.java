package com.linkmatm.service.emvDownload;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.linkmatm.service.Constants;
import com.linkmatm.service.R;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.device.DeviceInfo;
import com.paxsz.easylink.listener.FileDownloadListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileDownLoadActivity extends AppCompatActivity {
    private static final String TAG = FileDownLoadActivity.class.getSimpleName();

    private ListView fileListtt;
    private EasyLinkSdkManager manager;
    private ProgressDialog progressDialog;
    public static boolean iscancelfiledownload = false;

    ProgressDialog pDialog;
    String info = "";
    String clsInfo = "clss_param.clss", emvInfo = Constants.emvFile;

    LinearLayout parent;

    Thread thread;


    CardView clsCard, emvCard;
    LinearLayout clsLayout, emvLayout;
    TextView clsStatus, clsClick, emvStatus, emvClick;
    ProgressBar clsBar, clsLoader, emvBar, emvLoader;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filedownload);
        fileListtt = findViewById(R.id.fileList);
        Log.e(TAG, "onCreate: emv "+emvInfo );

        parent = findViewById(R.id.download_parent);


        clsCard = findViewById(R.id.cls_card);
        clsLayout = findViewById(R.id.cls_container);
        clsStatus = findViewById(R.id.cls_status);
        clsClick = findViewById(R.id.cls_download);
        clsBar = findViewById(R.id.cls_bar);
        clsLoader = findViewById(R.id.cls_progress);

        emvCard = findViewById(R.id.emv_card);
        emvLayout = findViewById(R.id.emv_container);
        emvStatus = findViewById(R.id.emv_status);
        emvClick = findViewById(R.id.emv_download);
        emvBar = findViewById(R.id.emv_bar);
        emvLoader = findViewById(R.id.emv_progress);


        emvCard.setVisibility(View.GONE);



        clsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadCls();
            }
        });

        emvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadEmv();
            }
        });

        manager = EasyLinkSdkManager.getInstance(this);
        progressDialog = new ProgressDialog(FileDownLoadActivity.this);


        final AssetManager assetManager = getAssets();
        try {
            // for assets folder add empty string
            String[] filelist = assetManager.list("paxfile");
            // for assets/subFolderInAssets add only subfolder name
            if (filelist == null) {
                // dir does not exist or is not a directory
            } else {
                for (int i = 0; i < filelist.length; i++) {
                    // Get filename of file or directory
                    String filename = filelist[i];
                    //  System.out.println(">>>----->>"+filename);
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, filelist);

            fileListtt.setAdapter(adapter);

        } catch (IOException e) {
            e.printStackTrace();
        }

        fileListtt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                info = ((TextView) view).getText().toString();
                Toast.makeText(getBaseContext(), "Item " + info, Toast.LENGTH_LONG).show();

                Log.e(TAG, "onItemClick: pos " + position + " item " + info);

                if (manager.isConnected(DeviceInfo.CommType.BLUETOOTH)) {
                    pDialog = new ProgressDialog(FileDownLoadActivity.this);
                    pDialog.setMessage("Downloading file. Please wait...");
                    pDialog.setIndeterminate(false);
                    pDialog.setMax(100);
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setCancelable(true);
                    pDialog.show();

/*
                    if (position == 0) {
                        Constants.clsParam = true;
                    } else if (position == 1) {
                        Constants.emvParam = true;
                    }
*/

                    new Thread(runnable).start();
                } else {
                    Toast.makeText(getApplicationContext(), "Please go to setting and connect your bluetooth device.", Toast.LENGTH_LONG).show();
                }

            }
        });
        checkPermission();

    }

    private void downloadCls() {
        info = clsInfo;
        if (manager.isConnected(DeviceInfo.CommType.BLUETOOTH)) {
            TransitionManager.beginDelayedTransition(clsLayout);

            clsLoader.setVisibility(View.VISIBLE);
            clsBar.setVisibility(View.VISIBLE);
            clsStatus.setText("Loading");
            clsStatus.setTextColor(getResources().getColor(R.color.colorAccent));
            clsCard.setClickable(false);
            clsClick.setText("PROCESSING");


//            pDialog = new ProgressDialog(FileDownLoadActivity.this);
//            pDialog.setMessage("Downloading file. Please wait...");
//            pDialog.setIndeterminate(false);
//            pDialog.setMax(100);
//            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            pDialog.setCancelable(true);
//            pDialog.show();
//            new Thread(runnable).start();
            thread = new Thread(runnable);
            thread.start();

        } else {
            Toast.makeText(getApplicationContext(), "Please go to setting and connect your bluetooth device.", Toast.LENGTH_LONG).show();
        }
    }

    private void downloadEmv() {
        info = emvInfo;
        if (manager.isConnected(DeviceInfo.CommType.BLUETOOTH)) {

            TransitionManager.beginDelayedTransition(clsLayout);

            emvLoader.setVisibility(View.VISIBLE);
            emvBar.setVisibility(View.VISIBLE);
            emvStatus.setText("Loading");
            emvStatus.setTextColor(getResources().getColor(R.color.colorAccent));
            emvCard.setClickable(false);
            emvClick.setText("PROCESSING");

//            pDialog = new ProgressDialog(FileDownLoadActivity.this);
//            pDialog.setMessage("Downloading file. Please wait...");
//            pDialog.setIndeterminate(false);
//            pDialog.setMax(100);
//            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            pDialog.setCancelable(true);
//            pDialog.show();
//            new Thread(runnable).start();
            thread = new Thread(runnable);
            thread.start();

        } else {
            Toast.makeText(getApplicationContext(), "Please go to setting and connect your bluetooth device.", Toast.LENGTH_LONG).show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(FileDownLoadActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                // ActivityCompat.requestPermissions(getActivity(), permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CAMERA_PERMISSIONS);
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 001);

            }
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(FileDownLoadActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getFilePath(info);

            Log.e(TAG, "getFilePath: cls " + Constants.clsParam);
            Log.e(TAG, "getFilePath: emv " + Constants.emvParam);

            try {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        Log.e(TAG, "run: "+Constants.clsParam+ "  "+Constants.emvParam );

                        if (Constants.clsParam){
                            TransitionManager.beginDelayedTransition(parent);
                            clsBar.setVisibility(View.GONE);
                            clsLoader.setVisibility(View.GONE);
                            clsStatus.setText("Completed");
                            clsStatus.setTextColor(getResources().getColor(R.color.green));
                            clsClick.setVisibility(View.GONE);


                            emvCard.setVisibility(View.VISIBLE);
                        }
                        if (Constants.emvParam){
                            try {
                                TransitionManager.beginDelayedTransition(parent);
                                emvBar.setVisibility(View.GONE);
                                emvLoader.setVisibility(View.GONE);
                                emvStatus.setTextColor(getResources().getColor(R.color.green));
                                emvStatus.setText("Completed");
                                emvClick.setVisibility(View.GONE);

                                finish();
                            } catch (Exception e){
                                Log.e(TAG, "run: exp "+e.getMessage() );
                            }

                        }
                    }
                });
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    };

    public int downLoadFile(String filename, String file_path) {
        // String file_path = "file:///android_asset/paxfile/ui_d180.ui"; //+ filename;

        int res = manager.fileDownLoad(file_path, new FileDownloadListener() {
            @Override
            public void onDownloadProgress(int current, int total) {
                Log.e(TAG, "onDownloadProgress: current " + current);
                Log.e(TAG, "onDownloadProgress: total " + total);
//                pDialog.setMax(total);

                if (info.equals(clsInfo)){
                    clsBar.setMax(total);
                } else if (info.equals(emvInfo)){
                    emvBar.setMax(total);
                }

                showDownloadProgress(current, total);

            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean cancelDownload() {
                return false;
            }
        });


        return res;
    }

    public void showDownloadProgress(int current_value, int total_size) {
        Double cCom = ((double) current_value / total_size) * 100;
        //int value = Math.round(Integer.valueOf(String.valueOf(cCom)));
        try {

            if (info.equals(clsInfo)) {
                if (current_value <= 24575) {
//                    pDialog.setProgress(current_value);

                    clsBar.setProgress(current_value);

                } else {
                    Constants.clsParam = true;

                    thread.interrupt();

                }
            } else if (info.equals(emvInfo)) {
                if (current_value <= 65535) {
                    emvBar.setProgress(current_value);
                } else {
                    Constants.emvParam = true;
                    thread.interrupt();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

            Log.e(TAG, "showDownloadProgress: "+e );



        }
    }

    public void getFilePath(String file_name) {
        AssetManager am = getAssets();

        try {
            InputStream inputStream = getAssets().open("paxfile/" + file_name);
            //  InputStream inputStream = am.open("/paxfile/"+file_name);
            File file = createFileFromInputStream(inputStream, file_name);
            int result = downLoadFile(file_name, file.getPath());



            Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    private File createFileFromInputStream(InputStream inputStream, String my_file_name) {

        try {

            File filepath = Environment.getExternalStorageDirectory();

            // System.out.println(filepath);


            File dir1 = new File(filepath.getAbsolutePath() + "/" + "PAX_DOWNLOADS" + "/");
            if (!dir1.exists()) {
                dir1.mkdirs();
            }

            File outfile = new File(dir1, my_file_name);


            // File f = new File(my_file_name);
            OutputStream outputStream = new FileOutputStream(outfile);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            inputStream.close();

            return outfile;
        } catch (IOException e) {
            //Logging exception
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onBackPressed() {
        if (Constants.clsParam){
            Toast.makeText(FileDownLoadActivity.this, "Please complete all the steps before exiting.", Toast.LENGTH_LONG).show();
        } else {
            super.onBackPressed();
        }
    }
}