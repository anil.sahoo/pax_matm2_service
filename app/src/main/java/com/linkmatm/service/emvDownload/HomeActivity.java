package com.linkmatm.service.emvDownload;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.linkmatm.service.BluetoothActivity;
import com.linkmatm.service.Constants;
import com.linkmatm.service.R;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private EasyLinkSdkManager manager;

    AlertDialog.Builder builder;

    TextView deviceStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        manager = EasyLinkSdkManager.getInstance(this);
        builder = new AlertDialog.Builder(HomeActivity.this);
        deviceStatus = findViewById(R.id.home_pair_status);

        findViewById(R.id.home_pair).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, EmvBluetoothActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.home_emv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (manager.isConnected()) {
                    Intent sIntent = new Intent(HomeActivity.this, FileDownLoadActivity.class);
                    startActivity(sIntent);
                } else {
                    Toast.makeText(HomeActivity.this, "Please pair the device", Toast.LENGTH_SHORT).show();
                    onResume();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "onResume: emv added " + Constants.emvParam);
        Log.e(TAG, "onResume: cls added " + Constants.clsParam);

        if (manager.isConnected()) {
            deviceStatus.setText("Device connected(" + manager.getConnectedDevice().getDeviceName() + ")");
            deviceStatus.setTextColor(getResources().getColor(R.color.green));
        } else {
            deviceStatus.setText("No device connected");
            deviceStatus.setTextColor(getResources().getColor(R.color.yellow));
        }


        if (Constants.emvParam && Constants.clsParam) {
            Log.e(TAG, "onResume: update dashboard");

            String msg = "Device has been refreshed. Please pair your bluetooth again from settings.";
            alert(msg);

        }

    }

    private void alert(final String statusDesc) {
        builder.setMessage(statusDesc);
        builder.setTitle("Success!");
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dialogDismiss();
                        onBackPressed();
                    }
                });
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

}