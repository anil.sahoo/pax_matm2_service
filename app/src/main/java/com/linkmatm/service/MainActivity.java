package com.linkmatm.service;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linkmatm.service.Utils.PreferenceUtility;
import com.linkmatm.service.emvDownload.HomeActivity;
import com.linkmatm.service.selfUpdate.DownloadActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.util.Map;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {
    int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String activityName = "";
    PreferenceUtility prefUtlity;
    EasyLinkSdkManager manager;
    ProgressDialog dialog;
    int EXTERNAL_STORAGE_PERMISSION_CODE = 23;
    String appCode = "", appLink = "";
    Bundle b;


    private static final int REQUEST_EXTERNAL_STORAGE = 25;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefUtlity = new PreferenceUtility(MainActivity.this);

        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        b = getIntent().getExtras();

        checkLocation();

    }

    public void configApp(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("Matm2Config").document(getPackageName());

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Log.e(TAG, "onComplete: executed");
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e(TAG, "onComplete: has data status " + task.getResult().getData());
                        Map<String, Object> packageMap = task.getResult().getData();
                        appCode = (String) packageMap.get("AppCode");
                        appLink = (String) packageMap.get("AppLink");
                        Constants.emvFile = (String) packageMap.get("EmvParam");

                        if (appCode.equals(getString(R.string.app_code))) {

                            if (b != null) {
                                activityName = b.getString("ActivityName");
                                if (activityName == null) {
                                    showAlertDialog("Invalid transaction data, Please use this app with parent application");

                                } else if (activityName.equals("Bluetooth")) {
                                    Constants.activityName = activityName;
                                    Constants.applicationType = b.getString("ApplicationType");
                                    if (Constants.applicationType.equals("CORE")) {
                                        Constants.token = b.getString("UserToken");
                                        Constants.user_id = b.getString("UserName");
                                    } else {
                                        Constants.loginID = b.getString("LoginID");
                                        Constants.encryptedData = b.getString("EncryptedData");

                                        String s = "Login_ID : "+Constants.loginID+"\nEncrypted Data : "+Constants.encryptedData;
                                        Log.e(TAG, "onComplete: s "+s );

                                    }
                                    Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else if (activityName.equals("mATM2")) {

                                    Constants.applicationType = b.getString("ApplicationType");
                                    Constants.latitude = String.valueOf(b.getDouble("latitude"));
                                    Constants.longitude = String.valueOf(b.getDouble("longitude"));
                                    Constants.activityName = activityName;
                                    if (Constants.applicationType.equals("CORE")) {
                                        Constants.token = b.getString("UserToken");
                                        Constants.user_id = b.getString("UserName");
                                        Constants.tokenFromCoreApp = b.getString("UserToken");
                                        Constants.userNameFromCoreApp = b.getString("UserName");
                                    } else {
                                        Constants.loginID = b.getString("LoginID");
                                        Constants.encryptedData = b.getString("EncryptedData");
                                        String s = "Login_ID : "+Constants.loginID+"\nEncrypted Data : "+Constants.encryptedData;
                                        Log.e(TAG, "onComplete: s "+s );
                                    }
                                    Constants.paramA = b.getString("ParamA");
                                    Constants.paramB = b.getString("ParamB");
                                    Constants.paramC = b.getString("ParamC");

                                    Constants.transactionType = b.getString("TransactionType");
                                    Constants.transactionAmount = b.getString("Amount");
                                    if (PosActivity.isBlueToothConnected(MainActivity.this)) {

                                        manager = EasyLinkSdkManager.getInstance(MainActivity.this);
                                        if (manager.isConnected()) {

                                            Intent intent = new Intent(MainActivity.this, PosActivity.class);
                                            startActivity(intent);
                                        } else {
                                            onBackPressed();
                                        }
                                    } else {


                                        // showAlertDialog("Please pair the bluetooth device");

                                        /*new changes*/
                                        Constants.applicationType = b.getString("ApplicationType");
                                        if (Constants.applicationType.equals("CORE")) {
                                            Constants.token = b.getString("UserToken");
                                            Constants.user_id = b.getString("UserName");
                                        } else {
                                            Constants.loginID = b.getString("LoginID");
                                            Constants.encryptedData = b.getString("EncryptedData");
                                        }
                                        Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                                        startActivityForResult(intent, 5000);
                                        //finish();

                                    }
                                } else if (activityName.equals("Download")){
                                    Constants.activityName = activityName;
                                    Constants.applicationType = b.getString("ApplicationType");
                                    if (Constants.applicationType.equals("CORE")) {
                                        Constants.token = b.getString("UserToken");
                                        Constants.user_id = b.getString("UserName");
                                    } else {
                                        Constants.loginID = b.getString("LoginID");
                                        Constants.encryptedData = b.getString("EncryptedData");
                                    }
                                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
                                    showAlertDialog("Invalid transaction data, Please use this app with parent application");
                                }
                            } else {
                                showAlertDialog("Invalid transaction data, Please use this app with parent application");
                            }
                        } else {

                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                verifyStoragePermissions();

                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.

                            } else {
                                Intent i = new Intent(MainActivity.this, DownloadActivity.class);
                                i.putExtra("AppLink", appLink);
                                startActivity(i);
                                finish();
                            }
                        }

                    }
                } else {
                    Log.e(TAG, "onComplete: error fetching data");
                    showAlertDialog("Failed to connect. Please try after sometimes.");
                }

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = Constants.intentData;
        if (Constants.isPos.equals(Constants.statusPos)) { //opens status activity in sdk
            setResult(RESULT_OK, i);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.error2Pos)) { //opens error2 activity in sdk
            setResult(RESULT_OK, i);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.errorPos)) { //opens error activity in sdk
            Intent in = new Intent();
            in.putExtra("error1Response", i.getIntExtra("errorResponse", 0));
            setResult(RESULT_OK, in);
            Constants.initResult();
            finish();
        } else if (Constants.isPos.equals(Constants.eventPos)) { //closes service and sdk
            Constants.initResult();
            finish();
        }
    }

    public void showAlertDialog(final String msg) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle("Alert")
                            .setMessage(msg)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    finishAffinity();
                                    System.exit(0);
                                }
                            })
                            .show();
                }
            });
        }
    }

    public void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: code "+requestCode );
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    //Request location updates:
                    forceUpdate();

                }

            } else {

                forceUpdate();
                // permission denied, boo! Disable the
                // functionality that depends on this permission.

            }
            return;
        } else if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            Log.e(TAG, "onRequestPermissionsResult: grant result "+grantResults.toString() );
            Log.e(TAG, "onRequestPermissionsResult: grant result at 0 "+grantResults[0] );
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onRequestPermissionsResult: app code "+appCode );
                Log.e(TAG, "onRequestPermissionsResult: app link "+appLink );
                if (!appCode.equals(getString(R.string.app_code))) {
                    Intent i = new Intent(MainActivity.this, DownloadActivity.class);
                    i.putExtra("AppLink", appLink);
                    startActivity(i);
                    finish();
                }
            }
        }

    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;

        new ForceUpdateAsync(currentVersion, MainActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                        showForceUpdateDialog(latestVersion);
                    } else {
                        configApp();
                    }
                } else {
                    configApp();
                }
            }
        }).execute();
    }

    public void showForceUpdateDialog(final String latestVersion) {

        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    String message = "A new version of this app is available. Please update to version " + latestVersion + " " + getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle(getResources().getString(R.string.youAreNotUpdatedTitle))
                            .setMessage(message)
                            .setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.linkmatm.service&hl=en_US");

                                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5000) {
            if (PosActivity.isBlueToothConnected(this)) {

                Intent intent = new Intent(MainActivity.this, PosActivity.class);
                startActivity(intent);
            } else {
                finish();
            }
        }
    }

    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}